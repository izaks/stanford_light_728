/**
 * Created by borisayupov on 12/8/16.
 */
(function($) {
  $(document).ready(function() {
    var $accordion = $('#accordion');
    var $row = $accordion.find('.views-row');
    var $title = $row.find('div:nth-child(2):not(.field-content)');
    var $body = $row.find('div:nth-child(3):not(.field-content)');
		var $image = $row.find('div:nth-child(1):not(.field-content)');

    $row.filter(":first").addClass('first').addClass("current");
    $row.filter(":last").addClass('last');
    $title.click(function() {
      if ($(this).parent().hasClass("current")) {
      } else {
        $row.filter('.current').children().eq(2).slideUp('slow', function () {
          $row.filter('.current').removeClass('current');
        });
        $(this).siblings('div:nth-child(3):not(.field-content)').slideDown('slow',function() {
          $(this).parent().addClass('current');
        });
      }
      return false;
    });
  });
})(jQuery);